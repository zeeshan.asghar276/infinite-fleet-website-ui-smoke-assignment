const imgLogo = "#if_logo"
const lnkError = "#error"
const txtH1Tag = "//h1"

class Error {
    clickErrorMenu() {
        cy.get(lnkError).click();
    }
    getH1Tag()
    {
       return cy.xpath(txtH1Tag);
    }
}
export default Error;