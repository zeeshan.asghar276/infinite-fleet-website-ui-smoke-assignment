const imgLogo = "#if_logo"
const TagH1 = "//h1"
const liForm = "//*[text()='Form']//ancestor::li"
const form = "#hello-form"
const txtName = "#hello-input"
const btnGo = "#hello-submit"

class Form {
    getInfinitFleetLogo() {
        return cy.get(imgLogo);
    }
    getHeaderTag(){
        return cy.xpath(TagH1);
    }
    getHomeButtonListItem() {
        return cy.xpath(liForm);
    }
    getForm(){
        return cy.get(form);
    }
    enterName(name){
        cy.get(txtName).type(name);
    }
    clickGoButton(){
        cy.get(btnGo).click();
    }
}
export default Form;