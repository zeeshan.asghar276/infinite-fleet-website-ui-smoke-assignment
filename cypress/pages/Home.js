const lnkHome = "#home"
const lnkForm = "#form"
const lnkUITesting = "#site"
const txtH1Tag  = "h1"
const imgLogo = "#if_logo"
const liHome = "//*[text()='Home']//ancestor::li"
const txtParagrpah = '.lead'

class Home {
    clickHomeMenu() {
      cy.get(lnkHome).click();
    }
    clickFormMenu() {
      cy.get(lnkForm).click();
    }
    clickUITestingMenu() {
      cy.get(lnkUITesting).click();
    }
    getH1Tag()
    {
       return cy.get(txtH1Tag);
    }
    getInfinitFleetLogo() {
      return cy.get(imgLogo);
    }
    getHomeButtonLink() {
      return cy.get(lnkHome);
    }
    getHomeButtonListItem() {
      return cy.xpath(liHome);
    }
    getParagraphText() {
      return cy.get(txtParagrpah);
    }
  }
  export default Home;