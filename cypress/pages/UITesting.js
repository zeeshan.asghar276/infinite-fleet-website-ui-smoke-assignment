const imgLogo = "#if_logo"

class UITesting {
    getInfinitFleetLogo() {
        return cy.get(imgLogo);
    }
}
export default UITesting;