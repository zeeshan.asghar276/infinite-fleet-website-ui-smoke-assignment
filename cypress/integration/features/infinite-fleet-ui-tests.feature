Feature: Infinite Fleet Website UI Smoke  - ENTRY TEST
    Background:
        Given I open Infinite Fleet Website

    Scenario: REQ-UI-01-The Title should be "UI Testing Site" on every site
        When I click on Home Menu item
        Then Page title should be "UI Testing Site"
        And I click on Form Menu item
        Then Page title should be "UI Testing Site"
        And I click on UI Testing link
        Then Page title should be "UI Testing Site"

    Scenario: REQ-UI-02-The Company Logo should be visible on every site
        When I click on Home Menu item
        Then Company Logo should be visible on home page
        And I click on Form Menu item
        Then Company Logo should be visible on form page
        And I click on UI Testing link
        Then Company Logo should be visible on UI Testing page

    Scenario: REQ-UI-03-When I click on the Home button, I should get navigated to the Home page
        When I click on Home Menu item
        Then I should get navigated to the Home page

    Scenario: REQ-UI-04-When I click on the Home button, it should turn to active status
        When I click on Home Menu item
        Then It should turn to active status

    Scenario: REQ-UI-05-When I click on the Form button, I should get navigated to the Form page
        When I click on Form Menu item
        Then I should get navigated to the Form page

    Scenario: REQ-UI-06-When I click on the Form button, it should turn to active status
        When I click on Form Menu item
        Then Form Menu item should turn to active status

    Scenario: REQ-UI-07-When I click on the Error button, I should get a 404 HTTP response code
        When I click on the Error button
        Then I should get a 404 HTTP response code
    
     Scenario: REQ-UI-08-When I click on the UI Testing button, I should get navigated to the Home page
        When I click on UI Testing link
        Then I should get navigated to the Home page
    
    Scenario: REQ-UI-09-The following text should be visible on the Home page in <h1> tag: "Welcome to Pixelmatic QA department"
          When I click on Home Menu item
          Then I should get navigated to the Home page
    
    Scenario: REQ-UI-10-The following text should be visible on the Home page in <h1> tag: "Welcome to Pixelmatic QA department"
          When I click on Home Menu item
          Then I should see the text "This site is dedicated to perform some exercises and demonstrate automated web testing."

    Scenario: REQ-UI-11-On the Form page, a form should be visible with one input box and one submit button
          When I click on Form Menu item
          Then I should see a form with one input box
          And  I should see a form with one submit button

    Scenario: REQ-UI-12-On the Form page, Enter value in input field and click Go, it should show "Hello" "Entered value"
          When I click on Form Menu item
          And  I enter name in the input field "Zeeshan"
          And  I click on go button
          Then It should show "Hello" space Entered Name "Zeeshan" on the redirected page
            