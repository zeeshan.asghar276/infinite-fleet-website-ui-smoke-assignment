import { Given } from "cypress-cucumber-preprocessor/steps";
import { When } from "cypress-cucumber-preprocessor/steps";
import { Then } from "cypress-cucumber-preprocessor/steps";
import { And } from "cypress-cucumber-preprocessor/steps";
import Home from "../../../pages/Home";
import UITesting from "../../../pages/UITesting";
import Form from "../../../pages/Form";
import Error from "../../../pages/Error";

const home = new Home();
const form = new Form();
const error = new Error();
const uitesting = new UITesting();
Given('I open Infinite Fleet Website', function () {
  cy.visit(Cypress.env("application_url"));
  cy.log(this.data.logoImgPath)
})

When('I click on Home Menu item', function () {
  home.clickHomeMenu();
})

Then('Page title should be {string}', (title) => {
  cy.title().should('include', title)
})

And('I click on Form Menu item', () => {
  home.clickFormMenu();
})

And('I click on UI Testing link', () => {
  home.clickUITestingMenu();
})

Then('Company Logo should be visible on home page', function () {
  cy.log(this.data.homePageWelcomeText)
  home.getInfinitFleetLogo().should('have.attr', 'src', this.data.logoImgPath);
})

Then('Company Logo should be visible on form page', function () {
  form.getInfinitFleetLogo().should('have.attr', 'src', this.data.logoImgPath);
})

Then('Company Logo should be visible on UI Testing page', function () {
  uitesting.getInfinitFleetLogo().should('have.attr', 'src', this.data.logoImgPath);
})

Then('I should get navigated to the Home page', function () {
  home.getH1Tag().should('have.text', this.data.homePageWelcomeText);
})

Then('It should turn to active status', () => {
  home.getHomeButtonListItem().should('have.attr', 'class', 'active');
})

Then('I should get navigated to the Form page', function () {
  form.getHeaderTag().should('have.text', this.data.formSubmissionText);
})

Then('Form Menu item should turn to active status', () => {
  form.getHomeButtonListItem().should('have.attr', 'class', 'active');
})

When('I click on the Error button', () => {
  error.clickErrorMenu();
})

Then('I should get a 404 HTTP response code', function () {
  error.getH1Tag().should('have.text', this.data.errorMessage);
})

Then('I should see the text {string}', (text) => {
  home.getParagraphText().should('have.text', text);
})

Then('I should see a form with one input box', () => {
  form.getForm().find("input").should('have.attr', 'id', 'hello-input');
})

And('I should see a form with one submit button', () => {
  form.getForm().find("button").should('have.attr', 'type', 'submit');
})

And('I enter name in the input field {string}', (name) => {
  form.enterName(name);
})

And('I click on go button', () => {
  form.clickGoButton();
})

Then('It should show {string} space Entered Name {string} on the redirected page', (hello, name) => {
  cy.xpath("//h1").should("have.text", hello + " " + name + "!");
})

