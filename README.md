# Infinite Fleet Website UI Smoke  - ENTRY TEST

Infinite Fleet Website UI Smoke  - ENTRY TEST

## Introduction

The project has been completed using cypress framework using java script language. 

## Project Structure

Below is the project structure. + represents the folder and - represents the file.

      +cypress
        +environments
          -dev.json
          -qa.json
        +fixtures         contains test data
          - infinite-fleet-ui-tests-data.json
        +integration
          +features       contains feature files/Test cases
            infinite-fleet-ui-tests.js
              -infinite-fleet-ui-tests-step-def.js
            -infinite-fleet-ui-tests.feature
        +Pages            contains page classed
          -Home.js
          -Error.js
          -UITesting.js
          -Form.js
        +plugins
          - index.js
        +support
          - commands.js
          - index.js
      -Cypress.json
      -Package.json
      -README.md
      -.gitignore

## Prerequisite
Install Nodejs (v14.7.0) in your system.


## How to Run the Project
    
    1- Open Visual Code IDE (You can use any other) and open terminal 
    2- clone the repository "https://gitlab.com/zeeshan.asghar276/infinite-fleet-website-ui-smoke-assignment.git"
    3- Go to project root directory where package.json is located
    4- Run command "npm install"

## Run Features/Tests in Headless Mode in Chrome

    npm run test --env configFile=dev

## Run Features/Tests in Headed Mode in Chrome

    npm run headTest --env configFile=dev


